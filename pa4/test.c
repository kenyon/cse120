#include "aux.h"
#include "umix.h"
#include "mythreads.h"

void print_lol(int t);
void print_wtf(int t);
void print_new(int t);

void Main()
{
	int retval, lol_tid[20], wtf_tid, new_tid;

	MyInitThreads();

	int main_id = MyGetThread();
	Printf("   Main(): MyGetThread() = %d\n", main_id);

	Printf("lolwut\n");

	//MyYieldThread(main_id);

	Printf("stfu\n");

	// 1
	Printf("\n   Main(): MySpawnThread(print_lol, 0)\n");
	lol_tid[0] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[0]);

	/*
	Printf("\n   Main(): MyYieldThread(%d)\n", lol_tid[0]);
	retval = MyYieldThread(lol_tid);
	Printf("   Main(): retval = %d\n", retval);

	Printf("\n   Main(): MyYieldThread(%d)\n", lol_tid[0]);
	retval = MyYieldThread(lol_tid);
	Printf("   Main(): retval = %d\n", retval);
	*/

	// 2
	Printf("\n   Main(): MySpawnThread(print_wtf, 0)\n");
	wtf_tid = MySpawnThread(print_wtf, 0);
	Printf("   Main(): wtf_tid = %d\n", wtf_tid);

	/*
	Printf("\n   Main(): MyYieldThread(%d)\n", wtf_tid);
	retval = MyYieldThread(wtf_tid);
	Printf("   Main(): retval = %d\n", retval);

	Printf("\n   Main(): MyYieldThread(%d)\n", wtf_tid);
	retval = MyYieldThread(wtf_tid);
	Printf("   Main(): retval = %d\n", retval);
	*/

	// 3
	Printf("   Main(): MySpawnThread(print_new, 0)\n");
	new_tid = MySpawnThread(print_new, 0);
	Printf("   Main(): new_tid = %d\n", new_tid);

	/*
	Printf("   Main(): MyYieldThread(%d)\n", new_tid);
	retval = MyYieldThread(new_tid);
	Printf("   Main(): retval = %d\n", retval);
	*/

	// 4
	Printf("   Main(): MySpawnThread(print_lol, 0)\n");
	lol_tid[1] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[1]);

	// 5
	Printf("   Main(): MySpawnThread(print_lol, 0)\n");
	lol_tid[2] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[2]);

	// 6
	Printf("   Main(): MySpawnThread(print_lol, 0)\n");
	lol_tid[3] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[3]);

	// 7
	Printf("   Main(): MySpawnThread(print_lol, 0)\n");
	lol_tid[4] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[4]);

	Printf("\n   Main(): MyYieldThread(%d)\n", lol_tid[4]);
	retval = MyYieldThread(lol_tid[4]);
	Printf("   Main(): retval = %d\n", retval);

	// 8
	Printf("\n   Main(): MySpawnThread(print_lol, 0)\n");
	lol_tid[5] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[5]);

	// 9
	Printf("   Main(): MySpawnThread(print_lol, 0)\n");
	//wtf_tid = MySpawnThread(do_wtf, 0);
	lol_tid[6] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[6]);

	//Exit();

	//MyYieldThread(lol_tid);

	// 10
	Printf("   Main(): MySpawnThread(print_lol, 0)\n");
	lol_tid[7] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[7]);

	// 11
	Printf("   Main(): MySpawnThread(print_lol, 0)\n");
	lol_tid[8] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[8]);

	// 12
	Printf("   Main(): MySpawnThread(print_lol, 0)\n");
	lol_tid[9] = MySpawnThread(print_lol, 0);
	Printf("   Main(): lol_tid = %d\n", lol_tid[9]);
}

void print_lol(int t)
{
	Printf("   print_lol(): calling MyExitThread()\n", t);
	MyExitThread();

	Printf("LOL!!!!!!1\n");
	Printf("   print_lol(): MyYieldThread(%d)\n", t);
	int retval = MyYieldThread(t);
	Printf("   print_lol(): retval = %d\n", retval);
}

void print_wtf(int t)
{
	Printf("   print_wtf(): calling MyExitThread()\n", t);
	MyExitThread();

	Printf("WTF!!!!!!2\n");
	Printf("   print_wtf(): MyYieldThread(%d)\n", t);
	int retval = MyYieldThread(t);
	Printf("   print_wtf(): retval = %d\n", retval);
}

void print_new(int t)
{
	Printf("NEW!!!!!!3\n");
	Printf("   print_new(): MyYieldThread(%d)\n", t);
	int retval = MyYieldThread(t);
	Printf("   print_new(): retval = %d\n", retval);

	Printf("   print_new(): calling MyExitThread()\n", t);
	MyExitThread();
}
