/**
 * Userland threading system.
 */

#include <stdio.h>
#include <setjmp.h>
#include "aux.h"
#include "umix.h"
#include "mythreads.h"

#define DEBUG 0
#define DEBUG_QUEUE 0
#define FALSE 0
#define STACKSIZE 65536 // Maximum size of thread stack.
#define TRUE 1

static int MyInitThreadsCalled = FALSE;	/* 1 if MyInitThreads called, else 0 */

// Current thread id. Starts at 0 and goes through MAXTHREADS - 1. Then it
// wraps around to 0. See increment_current_tid().
static int current_tid;

static struct thread {
	int tid; // Thread id. Should always match the index of the thread[] array.
	int valid; // 1 if entry is valid, else 0.
	int active; // 1 if entry is actively executing, else 0.
	int ran_once; // 1 if entry has executed for the first time, else 0.
	int last_yielder; // Thread id that last yielded to this thread. Initialized to -1.
	jmp_buf env; // Current context.
	jmp_buf init_env; // Initial context; only set once, during MyInitThreads().
	void (*func)();
	int param;
	struct thread *q_next; // Pointer to next queue item. "Next" means an item closer to the rear.
	struct thread *q_prev; // Pointer to prev queue item. "Prev" means an item closer to the front.
} thread[MAXTHREADS];

static struct thread* q_front;
static struct thread* q_rear;

// Function prototypes.
int MyInitThreads_not_called(void);
void MyInitThreads(void);
int MySpawnThread(void (*func)(), int param);
int MyYieldThread(int t);
int MyGetThread(void);
void MySchedThread(void);
void MyExitThread(void);
int next_thread_id(void);
void increment_current_tid(void);
void init_stack(int tnum);
void enqueue_thread(int tid);
int dequeue_thread(void);
void queue_show(void);
void queue_test(void);
int in_queue(int tid);
int queue_remove_thread(int tid);

/**
 * Returns TRUE if the given thread id is already in the queue, FALSE otherwise.
 */
int in_queue(int tid)
{
	if (!q_front) {
		return FALSE;
	}

	struct thread* threadp = q_front;

	do {
		if ((*threadp).tid == tid) {
			return TRUE;
		}
	} while ((threadp = (*threadp).q_next) != NULL);

	return FALSE;
}

/**
 * Places the given thread id at the rear of the queue.
 */
void enqueue_thread(int tid)
{
	if (tid > MAXTHREADS - 1 || tid < 0) {
		if (DEBUG) {Printf("enqueue_thread(%d): Warning: Ignored attempt to enqueue invalid thread id > MAXTHREADS - 1 or < 0.\n", tid);}
		return;
	}

	if (in_queue(tid)) {
		if (DEBUG) {Printf("enqueue_thread(%d): Warning: Ignored attempt to enqueue duplicate thread id.\n", tid);}
		return;
	}

	if (!q_front) {
		q_front = &thread[tid];
		q_rear = &thread[tid];
		return;
	}

	struct thread* old_rear = q_rear;
	(*q_rear).q_next = &thread[tid];
	q_rear = &thread[tid];
	thread[tid].q_prev = old_rear;
	thread[tid].q_next = NULL;
}

/**
 * Removes and returns the thread id at the front of the queue.
 * Returns -1 if the queue is empty.
 */
int dequeue_thread(void)
{
	if (!q_front) {
		return -1;
	}

	int front_tid = (*q_front).tid;

	q_front = (*q_front).q_next;

	if (q_front) {
		(*q_front).q_prev = NULL;
	}

	return front_tid;
}

/**
 * Removes a thread id from the queue, regardless of its position in the queue.
 * Returns the thread id removed.
 * Returns -1 if the queue is empty or the given id does not exist in the queue.
 */
int queue_remove_thread(int tid)
{
	// TODO: test this function

	if (!q_front) {
		return -1;
	}

	if (!in_queue(tid)) {
		return -1;
	}

	struct thread* threadp = q_front;

	do {
		if ((*threadp).tid == tid) {
			if (DEBUG) {Printf("queue_remove_thread(%d): saw %d in queue.\n", tid, (*threadp).tid);}

			if (DEBUG) {Printf("(*threadp).q_prev=%p\n", (*threadp).q_prev);}
			if (DEBUG) {Printf("(*threadp).q_next=%p\n", (*threadp).q_next);}

			// cases: tid is at front; tid is at rear; tid is somewhere in between

			// at front
			if (q_front == threadp) {
				return dequeue_thread();
			}

			// at rear
			if (q_rear == threadp) {
				struct thread* prevp = (*threadp).q_prev;
				(*prevp).q_next = (*threadp).q_next;
				q_rear = prevp;
				return tid;
			}

			// somewhere in between
			if ((*threadp).q_prev && (*threadp).q_next) {
				struct thread* prevp = (*threadp).q_prev;
				(*prevp).q_next = (*threadp).q_next;
				struct thread* nextp = (*threadp).q_next;
				(*nextp).q_prev = (*threadp).q_prev;
				return tid;
			}
		}
	} while ((threadp = (*threadp).q_next) != NULL);
}

void queue_show(void)
{
	if (!q_front) {
		Printf("\nqueue empty\n");
		return;
	}

	Printf("\n");

	struct thread* threadp = q_front;
	int i = 0;

	Printf("front: tid=%d\n", (*q_front).tid);

	do {
		Printf("%d: tid=%d\n", i, (*threadp).tid);

		i++;
	} while ((threadp = (*threadp).q_next) != NULL);

	Printf("rear: tid=%d\n", (*q_rear).tid);
	Printf("\n");
}

void queue_test(void)
{
	Printf("\nQUEUE TESTING\n");

	int tid = -1;

	queue_show();

	Printf("enqueuing 2\n");
	enqueue_thread(2);

	queue_show();

	tid = dequeue_thread();
	Printf("dequeued tid=%d\n", tid);

	queue_show();

	Printf("enqueuing 1, 4, 9, 10 (invalid, > MAXTHREADS - 1)\n");
	enqueue_thread(1);
	enqueue_thread(4);
	enqueue_thread(9);
	enqueue_thread(10);

	queue_show();

	Printf("4 in queue: %s\n", in_queue(4) ? "yes" : "no");
	Printf("5 in queue: %s\n", in_queue(5) ? "yes" : "no");
	Printf("\n");

	for (int i = 0; i < MAXTHREADS; i++) {
		Printf("enqueuing %d (q_front=%p, q_rear=%p)\n", i, q_front, q_rear);
		enqueue_thread(i);
	}

	queue_show();

	Printf("enqueuing 5 to a full queue\n");
	enqueue_thread(5);
	Printf("\n");

	for (int i = 0; i < MAXTHREADS; i++) {
		tid = dequeue_thread();
		Printf("%d: dequeued tid=%d (q_front=%p, q_rear=%p)\n", i, tid, q_front, q_rear);
	}

	queue_show();

	Printf("\nrequeuing\n\n");

	for (int i = 0; i < MAXTHREADS; i++) {
		Printf("enqueuing %d (q_front=%p, q_rear=%p)\n", i, q_front, q_rear);
		enqueue_thread(i);
	}

	queue_show();

	tid = dequeue_thread();
	Printf("dequeued tid=%d\n", tid);
	tid = dequeue_thread();
	Printf("dequeued tid=%d\n", tid);
	tid = dequeue_thread();
	Printf("dequeued tid=%d\n", tid);

	queue_show();

	Printf("enqueuing 1\n");
	enqueue_thread(1);

	queue_show();
}

/**
 * Prints an error message and calls Exit().
 */
int MyInitThreads_not_called(void)
{
	Printf("Must call MyInitThreads() first.\n");
	Exit();
	return 0; // Suppress compiler warning.
}

/**
 * MyInitThreads() initializes the thread package.  Must be the first function
 * called by any program that uses the thread package.
 */
void MyInitThreads(void)
{
	current_tid = 0;

	thread[0].tid = 0;
	thread[0].valid = TRUE; // Initial thread is 0.
	thread[0].active = TRUE; // Initial thread must be initially actively executing.
	thread[0].ran_once = TRUE;
	thread[0].last_yielder = -1;
	q_front = &thread[0];
	q_rear = &thread[0];

	init_stack(1);

	MyInitThreadsCalled = TRUE;

	if (DEBUG_QUEUE) {queue_test(); Exit();}
}

/**
 * Initializes the stack recursively.
 */
void init_stack(int tnum)
{
	if (tnum >= MAXTHREADS) {
		return;
	}

	//if (DEBUG) {Printf("      init_stack(): tnum = %d\n", tnum);}

	int jmpval;

	thread[tnum].tid = tnum;
	thread[tnum].valid = FALSE;
	thread[tnum].active = FALSE;
	thread[tnum].last_yielder = -1;
	thread[tnum].ran_once = FALSE;

	char s[STACKSIZE];

	if (DEBUG) {Printf("      tnum = %d, &s[STACKSIZE - 1] = %p, &s[0] = %p, diff = %d\n", tnum, &s[STACKSIZE - 1], &s[0], ((int) &s[STACKSIZE - 1]) - ((int) &s[0]) + 1);}

	if (((int) &s[STACKSIZE - 1]) - ((int) &s[0]) + 1 != STACKSIZE) {
		Printf("Stack space reservation failed.\n");
		Exit();
	}

	if ((jmpval = setjmp(thread[tnum].init_env)) == 0) {
		// direct setjmp call case
		if (DEBUG) {Printf("      MyInitThreads(): setjmp(thread[%d].init_env) == 0\n", tnum);}
	} else {
		// Executing thread with id jmpval.
		if (DEBUG) {Printf("      MyInitThreads(): setjmp(thread[%d].init_env) != 0; jmpval = %d\n", tnum, jmpval);}

		if (DEBUG) {Printf("      MyInitThreads(): (*thread[%d].func)(thread[%d].param); addrs: .func = %p, .param = %p\n", jmpval, jmpval, &thread[jmpval].func, &thread[jmpval].param);}
		(*thread[jmpval].func)(thread[jmpval].param);

		// Thread is done at this point.

		if (DEBUG) {Printf("      MyInitThreads(): here after func call: tnum=%d, jmpval=%d\n", tnum, jmpval);}

		// FIXME: WTF are we supposed to do here?

		MyExitThread();
	}

	init_stack(++tnum);
}

/**
 * Returns the next thread ID to be used.
 */
int next_thread_id(void)
{
	increment_current_tid();

	if (DEBUG) {Printf("      next_thread_id(): current_tid = %d\n", current_tid);}

	return current_tid;
}

/**
 * Increments current_tid.
 */
void increment_current_tid(void)
{
	int i, next = ++current_tid;

	// TODO: test this where we wrap around.
	if (current_tid == MAXTHREADS) {
		if (DEBUG) {Printf("      increment_current_tid(): current_tid == MAXTHREADS\n");}
		next = 0;
	}

	for (i = next; i < MAXTHREADS; i++) {
		if (!thread[i].valid) {
			if (DEBUG) {Printf("      increment_current_tid(): !thread[%d].valid\n", i);}
			current_tid = i;
			return;
		}
	}

	Printf("Error: Couldn't assign next thread id (MAXTHREADS in use).\n");
	Exit();
}

/**
 * MySpawnThread(func, param) spawns a new thread to execute func(param), where
 * func is a function with no return value and param is an integer parameter.
 * The new thread does not begin executing until another thread yields to it.
 * Returns the thread id of the newly spawned thread.
 */
int MySpawnThread(void (*func)(), int param)
{
	MyInitThreadsCalled || MyInitThreads_not_called();

	int next_tid = next_thread_id();
	thread[next_tid].valid = TRUE;
	//thread[next_tid].ran_once = FALSE;
	if (DEBUG) {Printf("      MySpawnThread(): next_tid = %d\n", next_tid);}

	if (DEBUG) {Printf("      MySpawnThread(): copying func and param\n");}
	thread[next_tid].func = func;
	thread[next_tid].param = param;

	if (DEBUG) {Printf("      MySpawnThread(): returning %d;\n", next_tid);}
	return next_tid;
}

/**
 * MyYieldThread(t) causes the running thread to yield to thread t.  Returns id
 * of thread that yielded to t (i.e., the thread that called MyYieldThread), or
 * -1 if t is an invalid id.
 */
int MyYieldThread(int t)
{
	MyInitThreadsCalled || MyInitThreads_not_called();

	if (!thread[t].valid) {
		Printf("Thread %d invalid.\n", t);
		return -1;
	}

	int caller_tid = MyGetThread();
	//if (DEBUG) {Printf("      MyYieldThread(%d): caller_tid = %d\n", t, caller_tid);}

	if (DEBUG) {Printf("      MyYieldThread(%d): thread[%d].last_yielder = %d\n", t, t, caller_tid);}
	thread[t].last_yielder = caller_tid;

	if (DEBUG) {Printf("      MyYieldThread(%d): thread[%d].active = 0\n", t, caller_tid);}
	thread[caller_tid].active = FALSE;

	if (DEBUG) {Printf("      MyYieldThread(%d): enqueue_thread(%d)\n", t, caller_tid);}
	enqueue_thread(caller_tid);

	if (DEBUG) {Printf("      MyYieldThread(%d): queue_remove_thread(%d)\n", t, t);}
	queue_remove_thread(t);

	if (setjmp(thread[caller_tid].env) == 0) {
		if (DEBUG) {Printf("      MyYieldThread(%d): setjmp(thread[%d].env) == 0\n", t, caller_tid);}

		if (DEBUG) {Printf("      MyYieldThread(%d): thread[%d].active = 1\n", t, t);}
		thread[t].active = TRUE;

		// go run the thread
		if (!thread[t].ran_once) {
			if (DEBUG) {Printf("      MyYieldThread(%d): thread[%d].ran_once == FALSE; longjmp(thread[%d].init_env, %d)\n", t, t, t, t);}
			thread[t].ran_once = TRUE;
			longjmp(thread[t].init_env, t);
		} else {
			if (DEBUG) {Printf("      MyYieldThread(%d): thread[%d].ran_once == TRUE; longjmp(thread[%d].env, %d)\n", t, t, t, t);}
			longjmp(thread[t].env, t);
		}
	} else {
		// Possible FIXME: don't know what to do here.

		if (DEBUG) {Printf("      MyYieldThread(%d): setjmp(thread[%d].env) != 0\n", t, caller_tid);}
		if (DEBUG) {Printf("      MyYieldThread(%d): now MyGetThread() = %d\n", t, MyGetThread());}
	}

	if (DEBUG) {Printf("      MyYieldThread(%d): returning thread[%d].last_yielder = %d\n", t, caller_tid, thread[caller_tid].last_yielder);}

	return thread[caller_tid].last_yielder;
}

/**
 * MyGetThread() returns id of currently running thread.
 */
int MyGetThread(void)
{
	MyInitThreadsCalled || MyInitThreads_not_called();

	// "Active" means "running" or "executing".
	int active_tid = -1, active_count = 0;

	for (int i = 0; i < MAXTHREADS; i++) {
		if (thread[i].active) {
			active_count++;
			active_tid = i;
			//break;
			// Theoretically, break could be uncommented and
			// active_count eliminated once I know this is working
			// correctly.
		}
	}

	//if (DEBUG) {Printf("      MyGetThread(): returning %d (active_count = %d).\n", active_tid, active_count);}

	if (active_count > 1) {
		Printf("Error: active_count should always be > 1 (it's %d).\n", active_count);
		Exit();
	}

	return active_tid;
}

/**
 * MySchedThread() causes the running thread to simply give up the CPU and
 * allow another thread to be scheduled.  Selecting which thread to run is
 * determined here.  Note that the same thread may be chosen (as will be the
 * case if there are no other threads).
 */
void MySchedThread(void)
{
	MyInitThreadsCalled || MyInitThreads_not_called();

	int caller_tid = MyGetThread();

	if (DEBUG) {Printf("      MySchedThread(): called by thread %d.\n", caller_tid);}

	// FIXME not done.

	// need to run the return value of dequeue_thread()
	// probably can't just call MyYieldThread()
}

/**
 * MyExitThread() causes the currently running thread to exit.
 */
void MyExitThread(void)
{
	MyInitThreadsCalled || MyInitThreads_not_called();

	int caller_tid = MyGetThread();

	if (DEBUG) {Printf("      MyExitThread(): called by thread %d.\n", caller_tid);}

	thread[caller_tid].valid = FALSE;
	thread[caller_tid].active = FALSE;
	thread[caller_tid].ran_once = FALSE;
	thread[caller_tid].last_yielder = -1;

	//FIXME should be chosen by MySchedThread()
	//if (DEBUG) {Printf("      MyExitThread(): longjmp(FIXME);\n");}

	if (DEBUG) {Printf("      MyExitThread(): MySchedThread();\n");}
	MySchedThread();

	//thread[0].active = TRUE;
	//longjmp(thread[0].env, 888);
}
