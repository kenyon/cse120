/* mykernel2.c: your portion of the kernel
 *
 *	Below are procedures that are called by other parts of the kernel.
 *	Your ability to modify the kernel is via these procedures.  You may
 *	modify the bodies of these procedures any way you wish (however,
 *	you cannot change the interfaces).
 * 
 */

#include "aux.h"
#include "sys.h"
#include "mykernel2.h"

#define TIMERINTERVAL 1	/* in ticks (tick = 10 msec) */

#define DEBUG 0

double CurrentCPUallocation(void);

/**
 * The process table.
 *
 * Remember to update anything using this structure if this structure changes.
 */
static struct {
	int valid;	 // Is this entry valid? 1 = yes, 0 = no
	int pid;	 // Process ID (as provided by kernel).
	int numerator;	 // Numerator of process's CPU rate.
	int denominator; // Denominator of process's CPU rate.
	int limit;	 // Normalized numerator. That is, the product of the numerator
			 // and the least common multiple of the denominators of all processes in proctab.
	int runs;	 // A revolving counter used for PROPORTIONAL. Is a number between 0 and limit.

	// The CPU rate numbers mean the process wants to run numerator out of
	// every denominator quanta. A quantum is the time between timer
	// interrupts, which is set by TIMERINTERVAL above.
} proctab[MAXPROCS];

/**
 * Returns the table index where a given process ID is located. Returns -1 if
 * the given process ID is not a valid entry in the process table.
 */
int ProcTabSlotOfPid(int pid)
{
	for (int i = 0; i < MAXPROCS; i++) {
		if (proctab[i].valid && proctab[i].pid == pid) {
			return i;
		}
	}

	return -1;
}

/**
 * Returns the number of valid entries in proctab.
 */
int ProcTabNumValid(void)
{
	int count = 0;

	for (int i = 0; i < MAXPROCS; i++) {
		if (proctab[i].valid) {
			count++;
		}
	}

	return count;
}

/**
 * Returns the number of invalid entries in proctab.
 */
int ProcTabNumInvalid(void)
{
	int count = 0;

	for (int i = 0; i < MAXPROCS; i++) {
		if (!proctab[i].valid) {
			count++;
		}
	}

	return count;
}

/**
 * Determines whether the process table is full. Returns 1 if full, 0
 * otherwise.
 */
int ProcTabIsFull(void)
{
	return ProcTabNumValid() == MAXPROCS;
}

/**
 * Determines whether the process table is empty. Returns 1 if empty, 0
 * otherwise.
 */
int ProcTabIsEmpty(void)
{
	return ProcTabNumValid() == 0;
}

/**
 * Copies a proctab entry from fromslot to toslot.
 */
void ProcTabCopyItem(int toslot, int fromslot)
{
	proctab[toslot].valid = proctab[fromslot].valid;
	proctab[toslot].pid = proctab[fromslot].pid;
	proctab[toslot].numerator = proctab[fromslot].numerator;
	proctab[toslot].denominator = proctab[fromslot].denominator;
	proctab[toslot].limit = proctab[fromslot].limit;
	proctab[toslot].runs = proctab[fromslot].runs;
}

/**
 * Compacts the process table, meaning invalid entries are removed and items
 * are shifted to make the table contiguous. Items are compacted toward index
 * 0. Returns 1 if proctab was changed, 0 if not, and -1 on error.
 */
int ProcTabCompact(void)
{
	int changed = 0;

	// First find out how many valid and invalid entries exist.
	int valids = ProcTabNumValid();
	int invalids = ProcTabNumInvalid();

	// Verify sanity.
	if (invalids + valids != MAXPROCS) {
		Printf("Error: something badly wrong in proctab (invalids+valids!=MAXPROCS).\n");
		return -1;
	}

	// Don't do anything if no invalid entries.
	if (invalids == 0) {
		return changed;
	}

	// The first invalid slot should be MAXPROCS - invalids. If it's not,
	// then we need to re-compact.
RECHECK:
	// Don't do anything if it's already compact.
	for (int i = MAXPROCS - invalids; i < MAXPROCS; i++) {
		if (proctab[i].valid) {
			goto NEEDSCOMPACTING;
		}
	}

	// This return is skipped if a valid entry is found where there should
	// be an invalid one.
	return changed;

NEEDSCOMPACTING:
	changed = 1;
	for (int i = 0, j; i < MAXPROCS; i++) {
		if (!proctab[i].valid) {
			for (j = i + 1; j < MAXPROCS; j++) {
				ProcTabCopyItem(j - 1, j);
			}
			proctab[MAXPROCS - 1].valid = 0;
			break;
		}
	}

	goto RECHECK;

	// This is ridiculous.
}

// Queue functions for proctab.
// Inserts go to index 0 of proctab, removals come from the other end.

/**
 * Inserts a process into the proctab queue. Returns 1 on sucess, 0 otherwise.
 */
int ProcTabInsert(int pid)
{
	if (ProcTabIsFull()) {
		return 0;
	}

	/*
	if (ProcTabCompact() == -1) {
		return 0;
	}
	*/

	// Shift entries up one.
	for (int i = MAXPROCS - 1; i >= 0; i--) {
		ProcTabCopyItem(i, i - 1);
	}

	proctab[0].valid = 1;
	proctab[0].pid = pid;
	proctab[0].numerator = 0;
	proctab[0].denominator = 0;
	proctab[0].limit = 0;
	proctab[0].runs = 0;

	return 1;
}

/**
 * Removes a process from the proctab queue. Returns the pid removed on sucess,
 * 0 otherwise.
 */
int ProcTabRemove(void)
{
	/*
	if (ProcTabCompact() == -1) {
		return 0;
	}
	*/

	int slot_to_remove = ProcTabNumValid() - 1;

	proctab[slot_to_remove].valid = 0;

	return proctab[slot_to_remove].pid;
}

/**
 * Returns the next pid in the proctab as if it were a queue. Returns 0 if
 * proctab is empty, or -1 on error.
 */
int ProcTabFrontPid(void)
{
	/*
	if (ProcTabCompact() == -1) {
		return -1;
	}
	*/

	if (ProcTabIsEmpty()) {
		return 0;
	}

	return proctab[ProcTabNumValid() - 1].pid;
}

// Stack functions for proctab.
// The top of the stack is index 0 of proctab.

/**
 * Pushes a process onto the proctab stack. Returns 1 on sucess, 0 otherwise.
 */
int ProcTabPush(int pid)
{
	if (ProcTabIsFull()) {
		return 0;
	}

	/*
	if (ProcTabCompact() == -1) {
		return 0;
	}
	*/

	// Shift entries up one.
	for (int i = MAXPROCS - 1; i >= 0; i--) {
		ProcTabCopyItem(i, i - 1);
	}

	proctab[0].valid = 1;
	proctab[0].pid = pid;
	proctab[0].numerator = 0;
	proctab[0].denominator = 0;
	proctab[0].limit = 0;
	proctab[0].runs = 0;

	return 1;
}

/**
 * Pops a process off the proctab stack. Returns the pid popped on sucess, 0
 * otherwise.
 */
int ProcTabPop(void)
{
	/*
	if (ProcTabCompact() == -1) {
		return 0;
	}
	*/

	int popped_pid = proctab[0].pid;

	proctab[0].valid = 0;

	if (ProcTabCompact() == -1) {
		return 0;
	}

	return popped_pid;
}

/**
 * Returns the pid of the top of the stack. That is, the next pid in the
 * proctab as if it were a stack. Returns 0 if proctab is empty, or -1 on
 * error.
 */
int ProcTabTopPid(void)
{
	/*
	if (ProcTabCompact() == -1) {
		return -1;
	}
	*/

	if (ProcTabIsEmpty()) {
		return 0;
	}

	return proctab[0].pid;
}

/**
 * InitSched () is called when kernel starts up.  Initialize data structures
 * (such as the process table) here.  Also, call any initialization procedures,
 * such as SetSchedPolicy to set the scheduling policy (see umix.h) and
 * SetTimer which sets the timer to interrupt after a specified number of
 * ticks.
 */
void InitSched(void)
{
	for (int i = 0; i < MAXPROCS; i++) {
		proctab[i].valid = 0;
		proctab[i].pid = -1;
		proctab[i].numerator = 0; // 0 means no specific allocation has been requested.
		proctab[i].denominator = 0;
		proctab[i].limit = 0;
		proctab[i].runs = 0;
	}
	//SetSchedPolicy (ARBITRARY);
	//SetSchedPolicy (FIFO);
	//SetSchedPolicy (LIFO);
	//SetSchedPolicy (ROUNDROBIN);
	SetSchedPolicy (PROPORTIONAL);
	SetTimer (TIMERINTERVAL);
}

/**
 * StartingProc (pid) is called by the kernel when the process identified by
 * pid is starting.  This allows you to record the arrival of a new process in
 * the process table, and allocate any resources (if necessary).  Returns 1 if
 * successful, 0 otherwise.
 */
int StartingProc(int pid)
{
	if (DEBUG) {Printf("[STARTING] pid=%d\n", pid);}
	switch (GetSchedPolicy()) {
		case FIFO:
			return ProcTabInsert(pid);

		case LIFO:
			// Calling DoSched() here is ecessary for LIFO for some
			// reason.  It looked like a good thing to try during
			// my testing, and it happened to work.
			DoSched(); 
			return ProcTabPush(pid);

		default:
			for (int i = 0; i < MAXPROCS; i++) {
				if (!proctab[i].valid) {
					proctab[i].valid = 1;
					proctab[i].pid = pid;
					proctab[i].numerator = 0;
					proctab[i].denominator = 0;
					proctab[i].limit = 0;
					proctab[i].runs = 0;

					return 1;
				}
			}

			Printf ("Error in StartingProc: no free table entries\n");
			return 0;
	}
}

/**
 * When slot zero is an invalid slot in the proctab, this procedure shifts the
 * other valid processes down to fill in that invalid slot. This way the
 * proctab acts like a FIFO queue.
 *
 * ProcTabCompact() should do the same thing as this.
 */
void ShiftProcsDown(void)
{
	// Don't do anything if the first slot isn't empty.
	if (proctab[0].valid) {
		return;
	}

	for (int i = 1; i < MAXPROCS; i++) {
		ProcTabCopyItem(i - 1, i);
	}
}

/**
 * Rotates the process table entries so that the first entry goes to the end
 * and the rest shift down.
 *
 * Example:
 *
 * Before RotateProcs():
 *
 * Index | Process
 *   0   | A
 *   1   | B
 *   2   | C
 *
 * After RotateProcs():
 *
 * Index | Process
 *   1   | B
 *   2   | C
 *   0   | A
 */
void RotateProcs(void)
{
	// Must store this here because the result of ProcTabNumValid() changes
	// during this procedure.
	int numvalidprocs = ProcTabNumValid();

	// Must be compacted for this to work reliably.
	if (ProcTabCompact() == -1) {
		Printf("Error compacting.\n");
		return;
	}

	//if (DEBUG) {Printf("in here, ProcTabNumValid=%d\n", numvalidprocs);}
	//if (DEBUG) {ShowProcTab();}
	// Save the first process's stuff.
	int first_valid = proctab[0].valid;
	int first_pid = proctab[0].pid;
	//if (DEBUG) {Printf("first_pid=%d\n", first_pid);}
	int first_num = proctab[0].numerator;
	int first_den = proctab[0].denominator;
	int first_limit = proctab[0].limit;
	int first_runs = proctab[0].runs;

	// Do the same as ShiftProcsDown() (can't use it because it won't do
	// anything if the first process isn't valid, and I want this rotation
	// function to be more general).
	for (int i = 1; i < MAXPROCS; i++) {
		//if (DEBUG) {Printf("[%d.%d] gets [%d.%d], ", i - 1, proctab[i - 1].pid, i, proctab[i].pid);}
		ProcTabCopyItem(i - 1, i);
	}

	// Put the first process's stuff in the last slot.
	proctab[numvalidprocs - 1].valid = first_valid;
	proctab[numvalidprocs - 1].pid = first_pid;
	//if (DEBUG) {Printf("[numvalidprocs - 1 = %d].pid=%d\n", numvalidprocs - 1, proctab[numvalidprocs - 1].pid);}
	proctab[numvalidprocs - 1].numerator = first_num;
	proctab[numvalidprocs - 1].denominator = first_den;
	proctab[numvalidprocs - 1].limit = first_limit;
	proctab[numvalidprocs - 1].runs = first_runs;
}

/**
 * EndingProc (pid) is called by the kernel when the process identified by pid
 * is ending.  This allows you to update the process table accordingly, and
 * deallocate any resources (if necessary).  Returns 1 if successful, 0
 * otherwise.
 */
int EndingProc(int pid)
{
	if (DEBUG) {Printf("[ENDING] pid=%d\n", pid);}

	int ending_slot = ProcTabSlotOfPid(pid);

	if (ending_slot == -1) {
		Printf ("Error in EndingProc: can't find process %d\n", pid);
		return 0;
	}

	switch (GetSchedPolicy()) {
		case FIFO:
			return ProcTabRemove() > 0;

		case LIFO:
			return ProcTabPop() > 0;

		default:
			proctab[ending_slot].valid = 0;
			//ShiftProcsDown();
			if (ProcTabCompact() == -1) {
				Printf("Error compacting.\n");
				return 0;
			}
			return 1;

	}
}

/**
 * Displays the process table. Only does anything if DEBUG is nonzero.
 */
void ShowProcTab(void)
{
	if (!DEBUG) {
		return;
	}

	for (int i = 0; i < MAXPROCS; i++) {
		Printf("   proctab[%d].valid=%d, .pid=%d, .num=%d, .den=%d, .runs=%d, .limit=%d\n",
				i, proctab[i].valid, proctab[i].pid, proctab[i].numerator, proctab[i].denominator,
				proctab[i].runs, proctab[i].limit);
	}
}

/**
 * SchedProc () is called by kernel when it needs a decision for which process
 * to run next.  It calls the kernel function GetSchedPolicy () which will
 * return the current scheduling policy which was previously set via
 * SetSchedPolicy (policy). SchedProc () should return a process id, or 0 if
 * there are no processes to run.
 */
int SchedProc(void)
{
	int nextpid = 0;

	switch (GetSchedPolicy ()) {
		case ARBITRARY:

			ShowProcTab();

			for (int i = 0; i < MAXPROCS; i++) {
				if (proctab[i].valid) {
					nextpid = proctab[i].pid;
					break;
				}
			}
			break;

		case FIFO:

			//ShowProcTab();

			/*
			//ShiftProcsDown();

			ProcTabCompact();
			if (!proctab[0].valid) {
				break;
			}
			nextpid = proctab[0].pid;
			*/
			nextpid = ProcTabFrontPid();

			break;

		case LIFO:

			//ShowProcTab();

			/*
			//ShiftProcsDown();
			ProcTabCompact();
			nextpid = proctab[NumValidProcs() - 1].pid;
			*/
			nextpid = ProcTabTopPid();

			break;

		case ROUNDROBIN:

			//ShowProcTab();

			//ShiftProcsDown();
			//ProcTabCompact();
			if (!proctab[0].valid) {
				break;
			}
			nextpid = proctab[0].pid;
			RotateProcs();

			break;

		case PROPORTIONAL:

			// PROPORTIONAL is basically the same as ROUNDROBIN
			// except it takes into account the requested CPU
			// rates. If no special CPU rates are requested, then
			// PROPORTIONAL == ROUNDROBIN.

			// Unallocated time is distributed evenly among
			// processes that have not requested rates, i.e., those
			// procs that have numerator and denominator = 0. Those
			// calculations are done in ProcTabSetLimits().

			if (!proctab[0].valid) {
				break;
			}

			if (DEBUG) {Printf("Current CPU allocation: %g%%\n", CurrentCPUallocation());}

			ShowProcTab();

			if (CurrentCPUallocation() > 0) {
				if (proctab[0].runs < proctab[0].limit) {
					nextpid = proctab[0].pid;
					if (DEBUG) {Printf("selected nextpid=%d\n", nextpid);}
					proctab[0].runs++;
					RotateProcs();
				} else { // proctab[0] reached its run limit.
					if (proctab[0].denominator != 0) {
						proctab[0].runs = 0;
					}

					do {
						if (DEBUG) {Printf("[PROP] rotating\n");}
						RotateProcs();
					} while (proctab[0].runs > proctab[0].limit);
					nextpid = proctab[0].pid;
					proctab[0].runs++;
				}
			} else { // Same as ROUNDROBIN.
				nextpid = proctab[0].pid;
				RotateProcs();
			}

			break;
	}
	
	if (DEBUG) {Printf(" returning pid %d\n", nextpid);}
	return nextpid;
}

/**
 * HandleTimerIntr () is called by the kernel whenever a timer interrupt
 * occurs.
 */
void HandleTimerIntr(void)
{
	SetTimer(TIMERINTERVAL);
	DoSched();
}

/**
 * Euclidean algorithm from Wikipedia.
 */
int gcd(int a, int b) 
{ 
	return b == 0 ? a : gcd(b, a % b);
}

/**
 * Calculates the lowest common denominator of the CPU rates.
 * lcm(a, b) = a * b / gcd(a, b)
 */
int ProcTabLCD(void)
{
	if (DEBUG) {Printf("ProcTabLCD() called, here's the proctab:\n");}
	ShowProcTab();

	int lcm = 0;

	// The lowest common denominator is the least common multiple of the
	// denominators.

	// Case: 1 valid proc. LCD = that single denominator.

	if (ProcTabNumValid() == 1) {
		for (int i = 0; i < MAXPROCS; i++) {
			if (proctab[i].valid) {
				return proctab[i].denominator;
			}
		}
	}

	// Case: >= 2 valid procs.

	// Get first denominator and assign it to lcm.
	int i;
	for (i = 0; i < MAXPROCS; i++) {
		if (proctab[i].denominator != 0 && proctab[i].valid) {
			lcm = proctab[i].denominator;
			break;
		}
	}

	for (i++; i < MAXPROCS; i++) {
		if (proctab[i].denominator != 0 && proctab[i].valid) {
			int gcdresult2 = gcd(proctab[i].denominator, lcm);
			if (DEBUG) {Printf("gcdresult2=%d\n", gcdresult2);}

			if (gcdresult2 != 0) {
				lcm = lcm * proctab[i].denominator / gcdresult2;
			}
		}

	}

	if (DEBUG) {Printf("returning lcm=%d\n", lcm);}

	return lcm;
}

/**
 * Return the number of valid processes in the process table that have not
 * requested specific CPU allocations.
 */
int ProcTabNumUnrequestedProcs(void)
{
	int count = 0;

	for (int i = 0; i < MAXPROCS; i++) {
		if (proctab[i].valid && proctab[i].numerator != 0 && proctab[i].denominator != 0) {
			count++;
		}
	}

	return count;
}

/**
 * Return the ceiling of a floating point number.
 */
int Ceiling(double num)
{
	return (int)(num + 0.999999);
}

/**
 * Calculate runtime limits for each process, that is, their normalized
 * numerators. Normalized numerator is the lowest common denominator divided by
 * the denominator times the numerator.
 */
void ProcTabSetLimits()
{
	int proctablcd = ProcTabLCD();
	int num_unrequested_procs = ProcTabNumUnrequestedProcs();

	for (int i = 0; i < MAXPROCS; i++) {
		if (proctab[i].valid && proctab[i].denominator != 0) {
			proctab[i].limit = proctablcd / proctab[i].denominator * proctab[i].numerator;
			if (DEBUG) {Printf("setting proctab[%d].limit=%d/%d*%d\n", i,
					proctablcd, proctab[i].denominator, proctab[i].numerator);}
		} else if (proctab[i].denominator == 0 && proctab[i].valid) {
			double unallocated = 100 - CurrentCPUallocation();
			if (DEBUG) {Printf("unallocated=%g%%, %d unrequested procs\n", unallocated, num_unrequested_procs);}
			if (DEBUG) {Printf("setting proctab[%d].limit=%g\n", i, unallocated / proctablcd / num_unrequested_procs);}
			proctab[i].limit = unallocated / proctablcd / num_unrequested_procs;
		}
	}
}

/**
 * Sets the CPU rate of a pid in the process table. Returns 1 on success, 0
 * otherwise.
 */
int SetCPUrate(int pid, int num, int den)
{
	if (DEBUG) {Printf("SetCPUrate(%d, %d, %d)\n", pid, num, den);}

	int pid_slot = ProcTabSlotOfPid(pid);

	if (pid_slot == -1) {
		return 0;
	}

	proctab[pid_slot].numerator = num;
	proctab[pid_slot].denominator = den;
	ProcTabSetLimits();
	proctab[pid_slot].runs = 0;

	return 1;
}

/**
 * Returns the current CPU time allocation. The return value is a fraction of
 * 100 (i.e., a percentage). For example, a return value of 50 means that 50%
 * of the CPU time has been allocated.
 */
double CurrentCPUallocation(void)
{
	double sum = 0;

	for (int i = 0; i < MAXPROCS; i++) {
		if (proctab[i].denominator != 0) {
			sum += (double)proctab[i].numerator/(double)proctab[i].denominator;
			//if (DEBUG) {Printf("num=%d, den=%d, sum=%g\n", proctab[i].numerator, proctab[i].denominator, sum);}
		}
	}

	//if (DEBUG) {Printf("CurrentCPUallocation() = %g\n", sum * 100);}
	return sum * 100;
}

/**
 * MyRequestCPUrate (pid, m, n) is called by the kernel whenever a process
 * identified by pid calls RequestCPUrate (m, n).  This is a request for a
 * fraction m/n of CPU time, effectively running on a CPU that is m/n of the
 * rate of the actual CPU speed.  m of every n quantums should be allocated to
 * the calling process.  MyRequestCPUrate (pid, m, n) should return 0 if
 * successful, i.e., if such a request can be satisfied, otherwise it should
 * return -1, i.e., error.
 */
int MyRequestCPUrate(int pid, int m, int n)
{
	if (m > n || m < 1 || n < 1 || CurrentCPUallocation() > 100) {
		return -1;
	}

	if (!SetCPUrate(pid, m, n)) {
		return -1;
	}

	return 0;
}
