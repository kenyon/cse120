/* Programming Assignment 3: Exercise D
 *
 * Now that you have a working implementation of semaphores, you can implement
 * a more sophisticated synchronization scheme for the car simulation.
 *
 * Study the program below.  Car 1 begins driving over the road, entering from
 * the East at 40 mph.  After 900 seconds, both Car 3 and Car 4 try to enter
 * the road.  Car 1 may or may not have exited by this time (it should exit
 * after 900 seconds, but recall that the times in the simulation are
 * approximate).  If Car 1 has not exited and Car 4 enters, they will crash
 * (why?).  If Car 1 has exited, Car 3 and Car 4 will be able to enter the
 * road, but since they enter from opposite directions, they will eventually
 * crash.  Finally, after 1200 seconds, Car 2 enters the road from the West,
 * and traveling twice as fast as Car 4.  If Car 3 was not coming from the
 * opposite direction, Car 2 would eventually reach Car 4 from the back and
 * crash.  (You may wish to experiment with reducing the initial delay of Car
 * 2, or increase the initial delay of Car 3, to cause Car 2 to crash with Car
 * 4 before Car 3 crashes with Car 4.)
 *
 * Exercises
 *
 * 1. Modify the procedure DriveRoad such that the following rules are obeyed:
 *
 *	A. Avoid all collisions.
 *
 *      B. If one car is already on the road, other cars traveling IN THE SAME
 *      DIRECTION SHOULD ENTER AT THE EARLIEST OPPORTUNITY, as long as there
 *      are no cars at the opposite end waiting.
 *
 *	C. If there are multiples cars on the road that entered at the same end
 *	(and thus traveling in the same direction), when a car arrives at the
 *	other end, it will have to wait.  It should only wait for the cars
 *	already on the road to exit; no new cars traveling in the same
 *	direction as the existing ones should be allowed to enter.  This
 *	implies that if there are multiple cars at each end waiting to enter
 *	the road, each side will take turns in allowing one car to enter and
 *	exit.  However, if there are no cars waiting at one end, then as cars
 *	arrive at the other end, they should be allowed to enter the road
 *	immediately.
 *	
 *	D. If the road is free (no cars), then any car attempting to enter
 *	should not be prevented from doing so.
 *
 *	E. All starvation must be avoided.  For example, any car that is
 *	waiting must eventually be allowed to proceed.
 *
 * This must be achieved ONLY by adding synchronization and making use of
 * shared memory (as described in Exercise C).  You should not modify the
 * delays or speeds to solve the problem.  In other words, the delays and
 * speeds are givens, and your goal is to enforce the above rules by making use
 * of only semaphores and shared memory.
 *
 * 2. Devise different tests (using different numbers of cars, speeds,
 * directions) to see whether your improved implementation of DriveRoad obeys
 * the rules above.
 *
 * IMPLEMENTATION GUIDELINES
 *
 * 1. Avoid busy waiting. In class one of the reasons given for using
 * semaphores was to avoid busy waiting in user code and limit it to minimal
 * use in certain parts of the kernel. This is because busy waiting uses up CPU
 * time, but a blocked process does not. You have semaphores available to
 * implement the driveRoad function, so you should not use busy waiting
 * anywhere.
 *
 * 2. Prevent race conditions. One reason for using semaphores is to enforce
 * mutual exclusion on critical sections to avoid race conditions.  You will be
 * using shared memory in your driveRoad implementation.  Identify the places
 * in your code where there may be race conditions (the result of a computation
 * on shared memory depends on the order that processes execute).  Prevent
 * these race conditions from occurring by using semaphores.
 *
 * 3. Implement semaphores fully and robustly.  It is possible for your
 * driveRoad function to work with an incorrect implementation of semaphores,
 * because controlling cars does not exercise every use of semaphores.  You
 * will be penalized if your semaphores are not correctly implemented, even if
 * your driveRoad works.
 *
 * 4. Avoid starvation.  This is especially relevant when implementing the
 * Signal function.  If there are multiple processes that blocked on the same
 * semaphore, then a good policy is to unblock them in FIFO order.
 *
 * 5. Control cars with semaphores: Semaphores should be the basic mechanism
 * for enforcing the rules on driving cars. You should not force cars to delay
 * in other ways inside driveRoad such as by calling the Delay function or
 * changing the speed of a car. (You can leave in the delay that is already
 * there that represents the car's speed, just don't add any additional
 * delaying.) Also, you should not be making decisions on what cars do using
 * calculations based on car speed (since there are a number of unpredictable
 * factors that can affect the actual cars' progress).
 *
 * GRADING INFORMATION
 *
 * 1. Semaphores: We will run a number of programs that test your semaphores
 * directly (without using cars at all). For example: enforcing mututal
 * exclusion, testing robustness of your list of waiting processes, calling
 * signal and wait many times to make sure the semaphore state is consistent,
 * etc.
 *
 * 2. Cars: We will run some tests of cars arriving in different ways, to make
 * sure that you correctly enforce all the rules for cars given in the
 * assignment.  We will use a correct semaphore implementation for these tests
 * so that even if your semaphores are not correct you could still get full
 * credit on the driving part of the grade.  Think about how your driveRoad
 * might handle different situations and write your own tests with cars
 * arriving in different ways to make sure you handle all cases correctly.
 *
 * WHAT TO TURN IN
 *
 * You must turn in two files: mykernel3.c and p3d.c.  mykernel3.c should
 * contain your implementation of semaphores, and p3d.c should contain your
 * modified version of InitRoad and DriveRoad (Main will be ignored).
 *
 * Your programs will be tested with various Main programs that will exercise
 * your semaphore implementation, AND different numbers of cars, directions,
 * and speeds, to exercise your DriveRoad function.  Our Main programs will
 * first call InitRoad before calling DriveRoad.  Make sure you do as much
 * rigorous testing yourself to be sure your implementations are robust.
 */

#include "aux.h"
#include "umix.h"

#define DEBUG 1

#define CAR1	1
#define CAR2	2
#define CAR3	3
#define CAR4	4

// Just to make it easier, I'm making roadsem indexes the same as the number of
// the road position, which means roadsem[0] is wasted.
#define NUMROADSEMS NUMPOS + 1

void InitRoad();
void driveRoad(int c, int from, int mph);

// Semaphores for syncrhonizing road access. Roadsem needs one semaphore per
// road position.
int roadsem[NUMROADSEMS], directional_sem;

// Semaphore for locking access to shared memory.
int mutex;

struct {
	// Origin of cars currently traveling on the road. Value is either WEST
	// or EAST, which are defined in umix.h.
	int from;

	// Number of cars waiting to enter the road.
	int waiters;

	// Number of cars on the road.
	int onroad;
} shm;

void Main()
{
	InitRoad();

    if(Fork() == 0){
        Delay(478);
        driveRoad(2, EAST, 84);
        Exit();
    }

    if(Fork() == 0){
        Delay(2361);
        driveRoad(3, EAST, 59);
        Exit();
    }

    if(Fork() == 0){
        Delay(1929);
        driveRoad(4, EAST, 11);
        Exit();
    }

    if(Fork() == 0){
        Delay(6148);
        driveRoad(5, EAST, 85);
        Exit();
    }

    if(Fork() == 0){
        Delay(2406);
        driveRoad(6, WEST, 44);
        Exit();
    }

    if(Fork() == 0){
        Delay(4672);
        driveRoad(7, WEST, 92);
        Exit();
    }

    if(Fork() == 0){
        Delay(9220);
        driveRoad(8, EAST, 2);
        Exit();
    }

    if(Fork() == 0){
        Delay(7589);
        driveRoad(9, EAST, 18);
        Exit();
    }

    if(Fork() == 0){
        Delay(2716);
        driveRoad(10, EAST, 50);
        Exit();
    }


    Delay(3497);
    driveRoad(1, EAST, 97);

    Exit();

	/*
	if (Fork() == 0) {				// Car 2
		//Delay(100);
		if (DEBUG) {Printf("CAR2 driving.\n");}
		driveRoad(CAR2, WEST, 40);
		if (DEBUG) {Printf("CAR2 exiting.\n");}
		Exit();
	}

	if (Fork() == 0) {				// Car 3
		//Delay(100);
		if (DEBUG) {Printf("CAR3 driving.\n");}
		driveRoad(CAR3, EAST, 50);
		if (DEBUG) {Printf("CAR3 exiting.\n");}
		Exit();
	}

	if (Fork() == 0) {				// Car 4
		//Delay(100);
		if (DEBUG) {Printf("CAR4 driving.\n");}
		driveRoad(CAR4, WEST, 30);
		if (DEBUG) {Printf("CAR4 exiting.\n");}
		Exit();
	}

	if (Fork() == 0) {				// Car 5
		Delay(9000);
		driveRoad(5, EAST, 30);

		if (Fork() == 0) {			// Car 6
			//Delay(17000);
			driveRoad(6, WEST, 30);
			Exit();
		}

		Exit();
	}

	if (DEBUG) {Printf("CAR1 driving.\n");}
	driveRoad(CAR1, EAST, 40);			// Car 1
	//driveRoad(CAR1, WEST, 40);			// Car 1

	if (DEBUG) {Printf("Main() exiting.\n");}

	Exit();
	*/
}

/**
 * Our tests will call your versions of InitRoad and DriveRoad, so your
 * solution to the car simulation should be limited to modifying the code
 * below. This is in addition to your implementation of semaphores contained in
 * mykernel3.c.
 */
void InitRoad()
{
	Regshm((char *) &shm, sizeof shm);

	shm.from = -1;
	shm.waiters = 0;
	shm.onroad = 0;

	mutex = Seminit(1);

	directional_sem = Seminit(0);

	for (int i = 1; i < NUMROADSEMS; i++) {
		roadsem[i] = Seminit(1);
	}
}

// Initial position.
#define IPOS(FROM)	(((FROM) == WEST) ? 1 : NUMPOS)

// Final position.
#define FPOS(FROM)	(((FROM) == WEST) ? NUMPOS : 1)

/**
 * Drives cars on a road and prevents crashes.
 *
 * A semaphore is used to prevent multiple cars traveling in opposite
 * directions from being on the road at the same time. Multiple cars might be
 * allowed on the road simultaneously if they are traveling in the same
 * direction.
 *
 * c is an arbitrary numerical car identifier.
 *
 * from is where the car should originate from: 0 for west or 1 for east. Also,
 * WEST and EAST are defined in umix.h.
 *
 * mph is the car's speed in miles per hour.
 */
void driveRoad(int c, int from, int mph)
{
	//if (DEBUG) {Printf("shm.from=%s\n", shm.from == WEST ? "WEST" : "EAST");}

	int position;

	// Prevent head-on collisions.

	Wait(mutex);
	if (shm.from != -1 && from != shm.from && shm.onroad > 0) {
		shm.waiters++;
		if (DEBUG) {Printf("c=%d Wait(directional_sem), shm.waiters=%d\n", c, shm.waiters);}
		Signal(mutex);
		Wait(directional_sem);
	} else {
		(shm.from == -1) && (shm.from = from);
		if (DEBUG) {Printf("c=%d shm.waiters=%d\n", c, shm.waiters);}
		if (shm.waiters > 0) {
			Signal(mutex);
			Wait(directional_sem);
		} else {
			//if (DEBUG) {Printf("shm.from=%s\n", shm.from == WEST ? "WEST" : "EAST");}
			Signal(mutex);
		}
	}

	// Entering road.

	Wait(mutex);
	shm.onroad++;
	Signal(mutex);

	if (DEBUG > 2) {Printf("Wait(roadsem[%d])\n", IPOS(from));}
	Wait(roadsem[IPOS(from)]);
	PrintRoad();
	Printf("Car %d entering at %d at %d mph\n", c, IPOS(from), mph);
	EnterRoad(from);
	PrintRoad();
	Printf("Car %d at %d\n", c, IPOS(from));

	// Driving on road.

	for (int i = 1; i < NUMPOS; i++) {
		(from == WEST) && (position = IPOS(from) + i);
		(from == EAST) && (position = IPOS(from) - i);

		if (i >= 2) {
			if (DEBUG > 2) {Printf("Signal(roadsem[%d])\n", from == WEST ? i - 1 : NUMPOS - i + 2);}
			Signal(roadsem[from == WEST ? i - 1 : NUMPOS - i + 2]);
		}

		if (DEBUG > 2) {Printf("Wait(roadsem[%d])\n", from == WEST ? i + 1 : NUMPOS - i);}
		Wait(roadsem[from == WEST ? i + 1 : NUMPOS - i]);

		Delay(3600/mph);
		if (DEBUG > 2) {Printf("Proceeding to %d.\n", from == WEST ? i + 1 : NUMPOS - i);}
		ProceedRoad();
		PrintRoad();
		Printf("Car %d at %d\n", c, position);
	}

	// Exiting road.

	if (DEBUG > 2) {Printf("Signal(roadsem[%d])\n", from == WEST ? FPOS(from) - 1 : FPOS(from) + 1);}
	Signal(roadsem[from == WEST ? FPOS(from) - 1 : FPOS(from) + 1]);
	Delay(3600/mph);
	ProceedRoad();
	PrintRoad();
	Printf("Car %d exiting at %d\n", c, FPOS(from));
	if (DEBUG > 2) {Printf("Signal(roadsem[%d])\n", FPOS(from));}
	Signal(roadsem[FPOS(from)]);

	Wait(mutex);
	shm.onroad--;
	Signal(mutex);

	//if (DEBUG) {Printf("After car %d exits road.\n", c);}

	// The second half of head-on collision prevention.

	Wait(mutex);
	if (DEBUG) {Printf("shm.waiters=%d, shm.onroad=%d\n", shm.waiters, shm.onroad);}
	if (shm.waiters > 0 && shm.onroad == 0) {
		shm.waiters--;
		if (DEBUG) {Printf("c=%d Signal(directional_sem), shm.waiters=%d\n", c, shm.waiters);}
		Signal(directional_sem);
	} else if (shm.waiters == 0 && shm.onroad == 0) { // FIXME: something not correct around here.
		//if (DEBUG) {Printf("c=%d Signal(directional_sem), shm.waiters=%d, shm.onroad=%d\n", c, shm.waiters, shm.onroad);}
		//Signal(directional_sem);
	} else if (shm.waiters < 0) {
		Printf("Woah! This should not happen!!!\n");
		Exit(1);
	}
	Signal(mutex);
}
