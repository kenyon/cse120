/**
 * mykernel.c: your portion of the kernel
 *
 * Below are procedures that are called by other parts of the kernel.  Your
 * ability to modify the kernel is via these procedures.  You may modify the
 * bodies of these procedures any way you wish (however, you cannot change the
 * interfaces).
 */

#include "aux.h"
#include "sys.h"
#include "mykernel3.h"

// DEBUG can be 0, 1, or 2. Higher number means more debugging output. 0 means
// no debugging output.
#define DEBUG 0
#define FALSE 0
#define TRUE 1

/**
 * Semaphore table.
 */
static struct {
	int valid;	// Is this a valid entry (was sem allocated)?
	int value;	// Value of semaphore.

	// List of processes blocked by this semaphore.  Slots are 0 if unused,
	// otherwise they are the pid of blocked processes.
	int proclist[MAXPROCS];
} semtab[MAXSEMS];

/**
 * InitSem() is called when the kernel starts up. Initialize data structures
 * (such as the semaphore table) and call any initialization procedures here.
 */
void InitSem()
{
	for (int s = 0; s < MAXSEMS; s++) {
		semtab[s].valid = FALSE;
		for (int i = 0; i < MAXPROCS; i++) {
			semtab[s].proclist[i] = 0;
		}
	}
}

/**
 * MySeminit(p, v) is called by the kernel whenever the system call Seminit(v)
 * is called. The kernel passes the initial value v, along with the process ID
 * p of the process that called Seminit. MySeminit should allocate a semaphore
 * (find a free entry in semtab and allocate), initialize that semaphore's
 * value to v, and then return the ID (i.e., index of the allocated entry).
 */
int MySeminit(int p, int v)
{
	int s;

	for (s = 0; s < MAXSEMS; s++) {
		if (semtab[s].valid == FALSE) {
			break;
		}
	}

	if (s == MAXSEMS) {
		Printf("No free semaphores\n");
		return -1;
	}

	semtab[s].valid = TRUE;
	semtab[s].value = v;

	return s;
}

/**
 * Displays the semaphore table.
 */
void ShowSemTab(void)
{
	if (DEBUG < 2) {
		return;
	}

	int limit = 1; // Arbitrary number of entries of semtab I want to see.

	for (int i = 0; i < limit; i++) {
		Printf("semtab[%d].valid=%d, .value=%d, .proclist=", i, semtab[i].valid, semtab[i].value);

		for (int j = 0; j < MAXPROCS; j++) {
			Printf("%d ", semtab[i].proclist[j]);
		}

		Printf("\n");
	}
}

/**
 * Blocks a process associated with a given semaphore.
 */
void BlockProc(int pid, int sem)
{
	for (int i = 0; i < MAXPROCS; i++) {
		if (semtab[sem].proclist[i] == 0) {
			semtab[sem].proclist[i] = pid;
			break;
		}
	}

	Block(pid);
}

/**
 * MyWait(pid, sem) is called by the kernel whenever the system call Wait(sem)
 * is called. pid is the pid of the calling process, sem is the semaphore
 * identifier.
 */
void MyWait(int pid, int sem)
{
	ShowSemTab();
	if (DEBUG) {Printf("MyWait(pid=%d, sem=%d)\n", pid, sem);}

	semtab[sem].value--;

	if (semtab[sem].value < 0) {
		BlockProc(pid, sem);
	}

	ShowSemTab();
}

/**
 * Copies a proclist entry from fromslot to toslot.
 */
void ProcListCopyItem(int sem, int toslot, int fromslot)
{
	semtab[sem].proclist[toslot] = semtab[sem].proclist[fromslot];
}

/**
 * In order for the blocked process list to be a FIFO queue, the list entries
 * need to be shifted whenever one is removed. This function does that for a
 * given semaphore in the semtab.
 */
void ShiftProcListEntriesDown(int sem)
{
	// Don't do anything if the first entry is a valid pid. This should
	// never be the case if this function is only called from
	// UnblockSomeProc().
	if (semtab[sem].proclist[0] != 0) {
		return;
	}

	for (int i = 1; i < MAXPROCS; i++) {
		ProcListCopyItem(sem, i - 1, i);
	}
}

/**
 * Unblocks at most one process associated with the given semaphore, if any
 * such processes exist.
 */
void UnblockSomeProc(int sem)
{
	int to_unblock = 0;

	for (int i = 0; i < MAXPROCS; i++) {
		if (semtab[sem].proclist[i] != 0) {
			to_unblock = semtab[sem].proclist[i];
			semtab[sem].proclist[i] = 0;
			ShiftProcListEntriesDown(sem);
			break;
		}
	}

	if (to_unblock) {
		Unblock(to_unblock);
	}
}

/**
 * MySignal(pid, sem) is called by the kernel whenever the system call
 * Signal(sem) is called. pid is the pid of the calling process, sem is the
 * semaphore identifier.
 */
void MySignal(int pid, int sem)
{
	ShowSemTab();
	if (DEBUG) {Printf("MySignal(pid=%d, sem=%d)\n", pid, sem);}

	semtab[sem].value++;

	if (semtab[sem].value <= 0) {
		UnblockSomeProc(sem);
	}

	ShowSemTab();
}
